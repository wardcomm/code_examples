#!/bin/bash
#
# Usage:  ./CreateCA.sh <Cluster Name>  <CA Pass>
#
export CLUSTER_NAME=$1
export CLUSTERCA="$CLUSTER_NAME"CA
export CLUSTER_TRUSTSTORE=$CLUSTERCA".ts"
export CAPASS=$2
export DIR="$CLUSTERCA"

if [ "x$CLUSTER_NAME" = "x" ]  ||  [ "x$CAPASS" = "x" ]; then
	echo "Missing args"
 	exit 1
fi

mkdir -p $DIR

openssl genrsa -out $DIR/$CLUSTERCA.key 2048 

openssl req -x509 -new -key $DIR/$CLUSTERCA.key -subj "/C=US/ST=CA/L=Cupertino/O=Apple/CN=corp.apple.com" -days 3650 -out $DIR/$CLUSTERCA.pem 

keytool -noprompt -importcert -alias $CLUSTERCA -file $DIR/$CLUSTERCA.pem -keystore $DIR/$CLUSTER_TRUSTSTORE -storepass $CAPASS

