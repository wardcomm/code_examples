#!/bin/bash
#set -x
#
# Args: <hostname> <keystore pass> <CA name> <truststore pass>
#
export HOSTNAME=$1
export PASS=$2
export HOSTNAME_S=`echo $1| awk -F\. '{print $1}'`

export CLUSTER_NAME=$3
export clusterCA="$CLUSTER_NAME"CA
export CAPASS=$4
export DIR=$CLUSTER_NAME/$HOSTNAME_S
export CADIR=$clusterCA

if [ ! -e $CADIR/$clusterCA.key ]; then
	echo "Cannot find CA $CADIR/$clusterCA.key"
	exit 1
fi
if [ ! -e $CADIR/$clusterCA.pem ]; then
	echo "Cannot find CA $CADIR/$clusterCA.key"
	exit 1
fi

mkdir -p $CLUSTER_NAME/$HOSTNAME_S

#Create cert and keystore
keytool -genkeypair -alias $HOSTNAME_S -keyalg RSA -keysize 1024 -dname "CN=$HOSTNAME,OU=Apple,O=Apple" -keypass $PASS -keystore $DIR/node.ks -storepass $PASS -validity 3650

#export certreq
keytool -keystore $DIR/node.ks -alias $HOSTNAME_S -certreq -file $DIR/$HOSTNAME.cert -storepass $PASS -keypass $PASS 

#Sign cert
openssl x509 -req -CA $CADIR/$clusterCA.pem -CAkey $CADIR/$clusterCA.key -in $DIR/$HOSTNAME.cert -out $DIR/$HOSTNAME.signed -days 3650 -CAcreateserial 

keytool -noprompt -keystore $DIR/node.ks -storepass $PASS -alias $clusterCA -import -file $CADIR/$clusterCA.pem 

keytool -noprompt -keystore $DIR/node.ks -storepass $PASS -alias $HOSTNAME_S -import -file $DIR/$HOSTNAME.signed -keypass $PASS 
