
1) Copy https://gcssvn.corp.apple.com/svn/linux/puppet/environments/hstack/gcsdba-hadoop/hadoop/config/dev/dbadev/hadoop-auth/hadoop-auth-3.0.0-SNAPSHOT.jar to /usr/lib/hadoop/lib
2) Copy https://gcssvn.corp.apple.com/svn/linux/puppet/environments/hstack/gcsdba-hadoop/hadoop/config/dev/dbadev/hadoop-auth/hadoop.auth.properties to /usr/lib/hadoop/conf
3Change core-site.xml:
add/replace property
  <property>
    <name>hadoop.http.authentication.type</name>
    <value>org.apache.hadoop.security.authentication.server.GCSAltKerberosAuthenticationHandler</value>

    <description>
    Defines authentication used for Oozie HTTP endpoint.
    Supported values are: simple | kerberos | #AUTHENTICATION_HANDLER_CLASSNAME#
  </description>
          


4) Change the hdfs-site.xml:
add/replace property

<property>
    <name>dfs.https.port</name>
    <value>50037</value>
              </property>
<property>

<property>
    <name>dfs.https.enable</name>
    <value>true</value>
        <description>Decide if HTTPS(SSL) is supported on HDFS
  </description>
</property>

<property>
    <name>dfs.https.server.keystore.resource</name>
    <value>ssl-server.xml</value>
        <description>Resource file from which ssl server keystore
  information will be extracted
  </description>
</property>

5) Configure SSL :

ssl-server.xml file is below
<configuration>
<property> 
 <name>ssl.server.truststore.type</name>
  <value>jks</value>
  <description>Optional. Default value is "jks".
  </description>
</property>

<property>
  <name>ssl.server.keystore.location</name>
  <value>/usr/lib/hadoop/conf/keystore.jks</value>
  <description>Keystore to be used by NN and DN. Must be specified.
  </description>
</property>

<property>
  <name>ssl.server.keystore.password</name>
  <value>welcome1</value>
  <description>Must be specified.
  </description>
</property>

<property>
  <name>ssl.server.keystore.keypassword</name>
  <value>welcome1</value>
  <description>Must be specified.
  </description>
</property>

<property>
  <name>ssl.server.keystore.type</name>
  <value>jks</value>
  <description>Optional. Default value is "jks".
  </description>
</property>

</configuration>



For development Create keystone and set the keystone path in /usr/lib/hadoop/conf/ssl-server.xml 
 
[hdfs@ma-gcsdbad-lnn01 GCSDBA DEV 20:00:36 /usr/lib/hadoop/conf]$ keytool -genkey -alias apple.com -keyalg RSA -keystore keystore.jks -keysize 2048
Enter keystore password:  
Re-enter new password: 
What is your first and last name?
  [Unknown]:  GCS DBA
What is the name of your organizational unit?
  [Unknown]:  GCSDBA
What is the name of your organization?
  [Unknown]:  apple 
What is the name of your City or Locality?
  [Unknown]:  Cupertino
What is the name of your State or Province?
  [Unknown]:  CA
What is the two-letter country code for this unit?
  [Unknown]:  US
Is CN=GCS DBA, OU=GCSDBA, O=apple, L=Cupertino, ST=CA, C=US correct?
  [no]:  yes

Enter key password for <apple.com>
	(RETURN if same as keystore password):  

For production, import SSL server certificate in keystone


