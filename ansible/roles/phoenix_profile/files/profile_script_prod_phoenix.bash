#!/bin/bash
#Varialbles
LOG_DIR_BASE=/ngs1/app
USER_LIST=/tmp/users_prod_phoenix.yml
PROFILE=profile_phoenix
#for loop
for i in `more $USER_LIST `
do
    echo $i
    ROGRP=`echo $i | sed 's/edwae/edwro/g'`
    cp /ngs/app/$i/.profile /ngs/app/$i/.profile_backup_"$(date +%Y-%m-%d-%H.%M.%S)"
    cp /tmp/$PROFILE /ngs/app/$i/.profile
    chown $i:$i /ngs/app/$i/.profile
    ls -ltr /ngs/app/$i/.profile
    mkdir -p $LOG_DIR_BASE/$i/log
    chmod 2750 $LOG_DIR_BASE/$i/log
    chown $i:$ROGRP $LOG_DIR_BASE/$i/log
    mkdir -p $LOG_DIR_BASE/$i/log/ClusterMaintenance
    mkdir -p $LOG_DIR_BASE/$i/log/CompactionEliminator
    mkdir -p $LOG_DIR_BASE/$i/log/HiveCompaction
    mkdir -p $LOG_DIR_BASE/$i/log/Compaction
    mkdir -p $LOG_DIR_BASE/$i/log/DataMigration
    mkdir -p $LOG_DIR_BASE/$i/log/ETLListener
    mkdir -p $LOG_DIR_BASE/$i/log/ETLListener
    mkdir -p $LOG_DIR_BASE/$i/log/FileToCore
    mkdir -p $LOG_DIR_BASE/$i/log/ObfuscateCopy
    mkdir -p $LOG_DIR_BASE/$i/log/DataReplication
    mkdir -p $LOG_DIR_BASE/$i/log/SemanticFwrkIP
    mkdir -p $LOG_DIR_BASE/$i/log/SemanticFwrk
    mkdir -p $LOG_DIR_BASE/$i/log/SqoopTool
    mkdir -p $LOG_DIR_BASE/$i/log/Autosys
    chmod 2750 $LOG_DIR_BASE/$i/log/*
    chown $i:$ROGRP $LOG_DIR_BASE/$i/log/*
    rm /ngs/app/$i/logs/log
    rm $LOG_DIR_BASE/$i/log
    rm  /ngs/app/$i/logs
    sudo -u $i ln -s /ngs1/app/$i/log /ngs/app/$i/logs
    sudo chown $i:$ROGRP /ngs/app/$i/logs

    ls -ltr $LOG_DIR_BASE/$i/log
    ls -ltr /ngs/app/$i/logs
done
