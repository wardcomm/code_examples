#!/bin/bash
#
## $1  paramter is for truststore password that exists already
#export truststorePassword=zdne4yI4LXDmv2IL
export truststoreDir=/ngs/app/hdfs/security
export truststoreFileName=node.ts
cd $truststoreDir
openssl s_client -showcerts -connect idmsauth.corp.apple.com:443 </dev/null 2>/dev/null|openssl x509 -outform PEM >appleconnecturl.pem

/usr/java/jdk64-1.7.0_75/bin/keytool -noprompt -importcert -alias appleconnect -file appleconnecturl.pem -keystore $truststoreDir/$truststoreFileName -storepass zdne4yI4LXDmv2IL
