ABSTRACT
       The primary aspect of this adhoc playbook is to create users.Before creating, it searches in LDAP for  existence of user.If the user doesn't nit exist in LDAP play is terminated with a failure message.if it exist it goes to the next step of kerberos check.if the kerberos check fails, it will generate a random keytab password,and goes with kerberos principle creation and email the password to user directly.Next it will create App directory and HDFS directory for user.

     And it takes the group names as input and modifies the groupfile by adding the user.

In scope: This currently handles only the named user creation and it is accepting only one user at a time. future scope will be to change the script to handle multiple users.
List of roles included in main.yml 

     1.AdhocUser.yml

          LDAP Search: In this it will check for LDAP user existed or not, if exist i will continue the execution flow and if not it will kill the flow.

          Checking for Keytab existance: This playbook will check for the keytab or kerberos and if existed it will skip generating random keytab password and keytab creation steps.I it fail to find that kerberos it will execute the next   steps to generate the kerberos ticket for security.

          Generating random keytab password & Keytab Creation: In this  it will generate the random keytab password and keytab creation which is used to connect securely.

          Email the password to the User: once the keytab and password is created it will email the password to the user directly,which is  specified in the input of that user.

          App directory creation: Finally once the user creation is done securely then directory will be created to that user.

2.AdhoUserGitPush.yml

3.AdhoUserGrp.yml:

          In the AdhocUserGroup clearly maps the lists of users associated to groups and super groups with which sub groups are associated

4.AdhoUser_bkp_latest.yml

5.Adhocgroupcheck.yml:

          In this adhocgroupcheck it wil check if the file exists in the groupfile and if not, it will fetch files from remote machine. 





DESIGN
ACTIVITY DIAGRAM / FLOW CHART
                   Flow chart needs to be design and yet to complete.

Mapping File:
               - which describes the mapping of netgroups and sub grooups which are associated to super group.This mapping file                   is  located in Adhocusercreation --->Default -->vi main.yml

SETUP / CONFIGURATION
               In this setup/configuration part we take the input from user such as :

                     1.user name/Id

                     2.Email Id

                     3.user group (To which group the user belong to)

                    4.Primary group

          And list of action that user can perform.

LIMITATIONS:
          In this play book the user actions are limited to access his own group and the associated groups based on the permission levels and action criteria . 

DESIGN alternatives: Not yet specified.
