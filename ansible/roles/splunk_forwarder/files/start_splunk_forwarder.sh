#!/bin/bash
# Prepared By Sreekanth Kanichetty
# Version 1.0
# 02-23-2016

SF_STATUS=$(ps -ef | grep -i splunk | grep -i "process-runner" | wc -l)
SF_HOME_DIR=/ngs/app/mapred/splunkforwarder8089

if [ -d "$SF_HOME_DIR" ]
then
     if [ $SF_STATUS == 0 ]
     then
	 echo "Started Splunk Forwarder"
         /ngs/app/mapred/splunkforwarder8089/bin/splunk start --accept-license
     fi
fi
exit 1
