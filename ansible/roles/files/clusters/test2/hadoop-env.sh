# Set Hadoop-specific environment variables here.

# The only required environment variable is JAVA_HOME.  All others are
# optional.  When running a distributed configuration it is best to
# set JAVA_HOME in this file, so that it is correctly defined on
# remote nodes.

# The java implementation to use.  Required.
export JAVA_HOME=/usr/java/jdk64-1.6.0_32
export HADOOP_HOME_WARN_SUPPRESS=1

# Hadoop Configuration Directory
export HADOOP_CONF_DIR=${HADOOP_CONF_DIR:-/etc/hadoop/conf}

# Heap sizes
export HADOOP_NAMENODE_HEAPSIZE="{{ HDP_NN_HEAP }}"
export HADOOP_JOBTRACKER_HEAPSIZE="{{ HDP_JT_HEAP }}"
export HADOOP_DATANODE_HEAPSIZE="{{ HDP_DN_HEAP }}"
export HADOOP_TASKTRACKER_HEAPSIZE="{{ HDP_TT_HEAP }}"
export HADOOP_CLIENT_HEAPSIZE="{{ HDP_CLNT_HEAP }}"

# Extra Java runtime options.  Empty by default.
export HADOOP_OPTS="-Djava.net.preferIPv4Stack=true ${HADOOP_OPTS}"

export HADOOP_GC_OPTS="-XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/ngs/app/hdfs/hadoop/logs/$USER/ -Xloggc:/ngs/app/hdfs/hadoop/logs/$USER/gc.log-`date +'%Y%m%d%H%M'` -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:ErrorFile=/ngs/app/hdfs/hadoop/logs/$USER/hs_err_pid%p.log"

# Command specific options appended to HADOOP_OPTS when specified
export HADOOP_NAMENODE_OPTS="-server -XX:ParallelGCThreads=8 -XX:CMSInitiatingOccupancyFraction=70 -XX:NewSize=1G -XX:MaxNewSize=1G  -Dhadoop.security.logger=INFO,DRFAS -Dhdfs.audit.logger=INFO,DRFAAUDIT ${HADOOP_NAMENODE_HEAPSIZE} ${HADOOP_GC_OPTS} ${HADOOP_OPTS} ${HADOOP_NAMENODE_OPTS}"
#HADOOP_JOBTRACKER_OPTS="-server -XX:ParallelGCThreads=8 -XX:CMSInitiatingOccupancyFraction=70 -XX:NewSize=200m -XX:MaxNewSize=200m  -Dhadoop.security.logger=INFO,DRFAS -Dmapred.audit.logger=INFO,MRAUDIT -Dhadoop.mapreduce.jobsummary.logger=INFO,JSA ${HADOOP_JOBTRACKER_HEAPSIZE} ${HADOOP_GC_OPTS} ${HADOOP_OPTS} ${HADOOP_JOBTRACKER_OPTS}"

HADOOP_JOBTRACKER_OPTS="  -Djavax.net.ssl.trustStore=/ngs/app/hdfs/security/node.ts -Djavax.net.ssl.trustStorePassword=zdne4yI4LXDmv2IL -Djavax.net.ssl.keyStore=/ngs/app/hdfs/security/node.ks -Djavax.net.ssl.keyStorePassword=ibDTzDrcnEHZjZB0  -server -XX:ParallelGCThreads=8 -XX:CMSInitiatingOccupancyFraction=70 -XX:NewSize=200m -XX:MaxNewSize=200m  -Dhadoop.security.logger=INFO,DRFAS -Dmapred.audit.logger=INFO,MRAUDIT -Dhadoop.mapreduce.jobsummary.logger=INFO,JSA ${HADOOP_JOBTRACKER_HEAPSIZE} ${HADOOP_GC_OPTS} ${HADOOP_OPTS} ${HADOOP_JOBTRACKER_OPTS}"


HADOOP_TASKTRACKER_OPTS="-server -Dhadoop.security.logger=ERROR,console -Dmapred.audit.logger=ERROR,console ${HADOOP_TASKTRACKER_HEAPSIZE} ${HADOOP_GC_OPTS} ${HADOOP_OPTS} ${HADOOP_TASKTRACKER_OPTS}"
HADOOP_DATANODE_OPTS="-Dhadoop.security.logger=ERROR,DRFAS ${HADOOP_OPTS} ${HADOOP_DATANODE_HEAPSIZE} ${HADOOP_GC_OPTS} ${HADOOP_DATANODE_OPTS}"
HADOOP_BALANCER_OPTS="-server -Xmx1024m ${HADOOP_BALANCER_OPTS}"

export HADOOP_SECONDARYNAMENODE_OPTS="-server -XX:ParallelGCThreads=8 -XX:NewSize=640m -XX:MaxNewSize=640m  -Dhadoop.security.logger=INFO,DRFAS -Dhdfs.audit.logger=INFO,DRFAAUDIT ${HADOOP_NAMENODE_HEAPSIZE} ${HADOOP_OPTS} ${HADOOP_GC_OPTS} ${HADOOP_SECONDARYNAMENODE_OPTS}"

# The following applies to multiple commands (fs, dfs, fsck, distcp etc)
export HADOOP_CLIENT_OPTS="${HADOOP_CLIENT_HEAPSIZE} ${HADOOP_CLIENT_OPTS}"
#HADOOP_JAVA_PLATFORM_OPTS="-XX:-UsePerfData ${HADOOP_JAVA_PLATFORM_OPTS}"

# On secure datanodes, user to run the datanode as after dropping privileges
export HADOOP_SECURE_DN_USER=hdfs

# Extra ssh options.  Empty by default.
export HADOOP_SSH_OPTS="-o ConnectTimeout=5 -o SendEnv=HADOOP_CONF_DIR"

# Where log files are stored.  $HADOOP_HOME/logs by default.
export HADOOP_LOG_DIR=/ngs/app/hdfs/hadoop/logs//$USER


# Where log files are stored in the secure data environment.
export HADOOP_SECURE_DN_LOG_DIR=/ngs/app/hdfs/hadoop/logs/$HADOOP_SECURE_DN_USER

# File naming remote slave hosts.  $HADOOP_HOME/conf/slaves by default.
# export HADOOP_SLAVES=${HADOOP_HOME}/conf/slaves

# host:path where hadoop code should be rsync'd from.  Unset by default.
# export HADOOP_MASTER=master:/home/$USER/src/hadoop

# Seconds to sleep between slave commands.  Unset by default.  This
# can be useful in large clusters, where, e.g., slave rsyncs can
# otherwise arrive faster than the master can service them.
# export HADOOP_SLAVE_SLEEP=0.1

# The directory where pid files are stored. /tmp by default.
export HADOOP_PID_DIR=/ngs/app/hdfs/hadoop/run//$USER
export HADOOP_SECURE_DN_PID_DIR=/ngs/app/hdfs/hadoop/run//$HADOOP_SECURE_DN_USER

# A string representing this instance of hadoop. $USER by default.
export HADOOP_IDENT_STRING=$USER

# The scheduling priority for daemon processes.  See 'man nice'.

# export HADOOP_NICENESS=10
export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:/ngs/app/hdfs/hadoop-auth/conf/hadoop-auth-gcsdba.jar