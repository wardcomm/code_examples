#!/bin/bash
# the script can be used to setup oozie with  apple connect authentication integration jar
chown -R oozie:oozie  /var/lib/oozie
/usr/lib/oozie/bin/oozie-setup.sh prepare-war   -jars /usr/lib/oozie/lib/mysql-connector-java-5.1.26-bin.jar:/usr/lib/hadoop/lib/hadoop-lzo-0.5.0.jar -hadoop 0.20.200  /usr/lib/hadoop -extjs /usr/share/HDP-oozie/ext-2.2.zip -secure
#-secure
/usr/lib/oozie/bin/oozied.sh start
sleep 120
/usr/lib/oozie/bin/oozied.sh stop

sleep 10
# copy lib file except those already exist
cp -inr /ngs/app/oozie/oozie_temp_lib/lib/* /var/lib/oozie/oozie-server/webapps/oozie/WEB-INF/lib/
chmod 644 /var/lib/oozie/oozie-server/webapps/oozie/WEB-INF/lib/*
chown oozie:oozie /var/lib/oozie/oozie-server/webapps/oozie/WEB-INF/lib/*

#/usr/lib/oozie/bin/oozied.sh start
sleep 100
chmod 644 /var/run/oozie/oozie.pid
chown oozie:oozie /var/run/oozie/oozie.pid
