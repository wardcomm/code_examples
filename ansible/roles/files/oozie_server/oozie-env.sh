#!/bin/bash
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Set Oozie specific environment variables here.

# Settings for the Embedded Tomcat that runs Oozie
# Java System properties for Oozie should be specified in this variable
#
export JAVA_HOME={{ JAVAHOME }}
export JAVA_LIBRARY_PATH="{{ JAVA_LIBRARY_PATH }}"

export OOZIE_CONFIG={{ OOZIE_CONFIG }}
export OOZIE_DATA={{ OOZIE_DATA }}
export OOZIE_LOG={{ OOZIE_LOG }}
export OOZIE_TMP_DIR={{ OOZIE_TMP_DIR }}
export OOZIE_CONFIG_FILE={{ OOZIE_CONFIG_FILE }}
export OOZIE_LOG4J_FILE={{ OOZIE_LOG4J_FILE }}

export OOZIE_HTTP_HOSTNAME={{ OOZIE_HTTP_HOSTNAME }}
export CATALINA_BASE={{ CATALINA_BASE }}
export CATALINA_PID={{ CATALINA_PID }}
export CATALINA_TMP_DIR={{ CATALINA_TMP_DIR }}
export OOZIE_HTTP_PORT={{ OOZIE_HTTP_PORT }}
export OOZIE_HTTPS_PORT={{ OOZIE_HTTPS_PORT }}
export OOZIE_URL="{{ OOZIE_URL }}"
export OOZIE_BASE_URL="{{ OOZIE_BASE_URL }}"

export OOZIE_CLIENT_OPTS="{{ OOZIE_CLIENT_OPTS }}"
export OOZIE_HTTPS_KEYSTORE_FILE={{ OOZIE_HTTPS_KEYSTORE_FILE }}
export OOZIE_HTTPS_KEYSTORE_PASS={{ OOZIE_HTTPS_KEYSTORE_PASS }}