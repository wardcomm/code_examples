# Set Hadoop-specific environment variables here.

# The only required environment variable is JAVA_HOME.  All others are
# optional.  When running a distributed configuration it is best to
# set JAVA_HOME in this file, so that it is correctly defined on
# remote nodes.

# The java implementation to use.  Required.
export JAVA_HOME={{ JAVAHOME }}
export HADOOP_HOME_WARN_SUPPRESS={{ HADOOP_HOME_WARN_SUPPRESS }}

# Hadoop Configuration Directory
export HADOOP_CONF_DIR=${HADOOP_CONF_DIR:-/etc/hadoop/conf}

# Heap sizes
export HADOOP_NAMENODE_HEAPSIZE="{{ HDP_NN_HEAP }}"
export HADOOP_JOBTRACKER_HEAPSIZE="{{ HDP_JT_HEAP }}"
export HADOOP_DATANODE_HEAPSIZE="{{ HDP_DN_HEAP }}"
export HADOOP_TASKTRACKER_HEAPSIZE="{{ HDP_TT_HEAP }}"
export HADOOP_CLIENT_HEAPSIZE="{{ HDP_CLNT_HEAP }}"
export HADOOP_JOB_HISTORYSERVER_HEAPSIZE="{{ HADOOP_JOB_HISTORYSERVER_HEAPSIZE }}"
# Extra Java runtime options.  Empty by default.
export HADOOP_OPTS="{{ HADOOP_OPTS }}"
export HADOOP_GC_OPTS="{{ HADOOP_GC_OPTS }}"

# Command specific options appended to HADOOP_OPTS when specifiec
export HADOOP_NAMENODE_OPTS="{{ HADOOP_NAMENODE_OPTS }}"
export HADOOP_JOBTRACKER_OPTS="{{ HADOOP_JOBTRACKER_OPTS }}"
export HADOOP_TASKTRACKER_OPTS="{{ HADOOP_TASKTRACKER_OPTS }}"
export HADOOP_DATANODE_OPTS="{{ HADOOP_DATANODE_OPTS }}"
export HADOOP_BALANCER_OPTS="{{ HADOOP_BALANCER_OPTS }}"

export HADOOP_SECONDARYNAMENODE_OPTS="{{ HADOOP_SECONDARYNAMENODE_OPTS }}"

# The following applies to multiple commands (fs, dfs, fsck, distcp etc)
export HADOOP_CLIENT_OPTS="{{ HADOOP_CLIENT_OPTS }}"
#HADOOP_JAVA_PLATFORM_OPTS="{{ HADOOP_JAVA_PLATFORM_OPTS }}"

# On secure datanodes, user to run the datanode as after dropping privileges
export HADOOP_SECURE_DN_USER={{ HADOOP_SECURE_DN_USER }}

# Extra ssh options.  Empty by default.
export HADOOP_SSH_OPTS="{{ HADOOP_SSH_OPTS }}"

# Where log files are stored.  $HADOOP_HOME/logs by default.
export HADOOP_LOG_DIR={{ HADOOP_LOG_DIR }}


# Where log files are stored in the secure data environment.
export HADOOP_SECURE_DN_LOG_DIR={{ HADOOP_SECURE_DN_LOG_DIR }}

# File naming remote slave hosts.  $HADOOP_HOME/conf/slaves by default.
# export HADOOP_SLAVES={{ HADOOP_SLAVES }}

# host:path where hadoop code should be rsync'd from.  Unset by default.
# export HADOOP_MASTER={{ HADOOP_MASTER }}

# Seconds to sleep between slave commands.  Unset by default.  This
# can be useful in large clusters, where, e.g., slave rsyncs can
# otherwise arrive faster than the master can service them.
# export HADOOP_SLAVE_SLEEP={{ HADOOP_SLAVE_SLEEP }}

# The directory where pid files are stored. /tmp by default.
export HADOOP_PID_DIR={{ HADOOP_PID_DIR }}
export HADOOP_SECURE_DN_PID_DIR={{ HADOOP_SECURE_DN_PID_DIR }}

# A string representing this instance of hadoop. $USER by default.
export HADOOP_IDENT_STRING={{ HADOOP_IDENT_STRING }}

# The scheduling priority for daemon processes.  See 'man nice'.

# export HADOOP_NICENESS={{ HADOOP_NICENESS }}

# AppleConnect Integration 
export HADOOP_CLASSPATH={{ HADOOP_CLASSPATH }}
