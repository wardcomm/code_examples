#!/bin/bash

#this is the shell script to start MetaStore . usage
# $install_dir/scripts/startMetastore.sh 

user=`whoami`
source ~/.profile
export LANG=en_US.UTF-8


function getParentDirPath() {
    pushd "$PWD" > /dev/null
    dir=`dirname $0`
    if [ $dir == "." ]; then
        cd ..
        path=$PWD
    else
        path=`dirname $dir`
    fi
    popd > /dev/null
    PARENT_DIR=$path
}

#get application root directory
getParentDirPath $0
cd $PARENT_DIR
HOME_DIR=`pwd`
#echo $HOME_DIR

#F2C_LIB_DIR=/ngs/app/edwcrfwp/app/FileToCore-2.2.0/lib
F2C_LIB_DIR={{ F2C_LIB_DIR }}
PX4_LIB_DIR={{ PX4_LIB_DIR }}

if [ "${F2C_LIB_DIR}" == "" ] ; then  F2C_LIB_DIR=$HOME_DIR/lib ; fi

CLASSPATH=${CLASSPATH}:"."

# add libs to CLASSPATH
for f in $F2C_LIB_DIR/*.jar; do
        CLASSPATH=${CLASSPATH}:$f
done

export HADOOP_CLASSPATH=$CLASSPATH

if [ -f /usr/kerberos/sbin/kinit ]; then
KINIT_LOCATION="/usr/kerberos/sbin/kinit"
elif [ -f /usr/kerberos/bin/kinit ]; then
KINIT_LOCATION="/usr/kerberos/bin/kinit"
elif [ -f /usr/bin/kinit ]; then
KINIT_LOCATION="/usr/bin/kinit"
elif [ -f /usr/sbin/kinit ]; then
KINIT_LOCATION="/usr/sbin/kinit"
else
KINIT_LOCATION="kinit location not found"
fi


${KINIT_LOCATION} -k -t /etc/security/keytab/hive.service.keytab -p hive/{{ ansible_fqdn }}@{{ REALM }}
#normal metastore start command : hive --service metastore
hive --service metastore