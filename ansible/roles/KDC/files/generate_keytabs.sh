#!/bin/bash

#password as argument

pass=$1

# Set this to the name of your Kerberos realm.

krb_realm=HADOOP.GCSKDCT.CORP.APPLE.COM

## naviagate to keytab

cd /etc/security/keytab/

host=`hostname`

kadmin=`locate kadmin | grep -v man`

isLap=$(hostname | grep "lap")

if [ $isLap ]; then

echo "secret" >> http-secret

## reads all delimited ":" "idm" users from /etc/passwd

for name in `cat /etc/passwd | grep idm[ae,ro] | cut -d ":" -f 1`; do

## authenticate  to KDC server ,where 'w' argument reads password rather than stdin

${kadmin} -p gcsdbahadoop/dba@${krb_realm} -w $pass  <<EOF

addprinc -randkey ${name}/${host}@${krb_realm}

ktadd -k ${name}.service.keytab  ${name}/${host}@${krb_realm}

EOF

done

fi

isNam=$(hostname | grep "lnn")

if [ $isNam ]; then

echo "secret" >> http-secret

## reads all users from array

array=(nn jt)

for name in "${array[@]}"

do

## authenticate  to KDC server ,where 'w' argument reads password rather than stdin

${kadmin} -p gcsdbahadoop/dba@${krb_realm} -w $pass  <<EOF

addprinc -randkey ${name}/${host}@${krb_realm}

ktadd -k ${name}.service.keytab  ${name}/${host}@${krb_realm}

EOF

done

${kadmin} -p gcsdbahadoop/dba@${krb_realm} -w $pass  <<EOF

addprinc -randkey HTTP/${host}@${krb_realm}

ktadd -k spnego.service.keytab  HTTP/${host}@${krb_realm}

EOF

fi

isDat=$(hostname | grep "ldn")

if [ $isDat ]; then

echo "secret" >> http-secret

## reads all users from array

array=(dn tt)

for name in "${array[@]}"

do

## authenticate  to KDC server ,where 'w' argument reads password rather than stdin

${kadmin} -p gcsdbahadoop/dba@${krb_realm} -w $pass  <<EOF

addprinc -randkey ${name}/${host}@${krb_realm}

ktadd -k ${name}.service.keytab  ${name}/${host}@${krb_realm}

EOF

done

fi

### change permissions on all *.keytab files to 444
chmod 444 *

exit
