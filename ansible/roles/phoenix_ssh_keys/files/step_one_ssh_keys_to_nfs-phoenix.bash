#!/bin/bash
#For all EDWAE* user on ma2-gbip-lap101 run below
#variables
legacy=/ngs/app
nfs=/ngs150
cluster=PHOENIX
tmp_ssh_keys=102_keys
backup=BACKUP
date=$(date +%d%m%y%H%M)

#for loop
for i in `more /tmp/phoenix_edw_prod.yml`
do
    echo $i
mkdir -p $nfs/hadoop/$cluster/$i/
mkdir -p $nfs/hadoop/$cluster/$i/$backup
mkdir -p $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/
chmod 600 $legacy/$i/.ssh/id_rsa
chmod 600 $legacy/$i/.ssh/id_rsa.pub
chmod 700 $legacy/$i/.ssh
chown -R $i:$i $legacy/$i/.ssh/
chown -R $i:$i $legacy/$i/.ssh/id_rsa
chown -R $i:$i $legacy/$i/.ssh/id_rsa.pub
cp $legacy/$i/.ssh/id_rsa $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/id_rsa
cp $legacy/$i/.ssh/id_rsa.pub $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/id_rsa.pub
cp $legacy/$i/.profile $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/.profile
chown -R $i:$i $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/id_rsa
chown -R $i:$i $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/id_rsa.pub
chown -R $i:$i $nfs/hadoop/$cluster/$i/$backup/"$(date)"/$tmp_ssh_keys/.profile
ls -ltr $nfs/hadoop/$cluster/$i/$backup/$tmp_ssh_keys/id*
cp $legacy/$i/.ssh/id_rsa nfs/hadoop/$cluster/edwsftpp/$backup/"$(date)"/$i/id_rsa
cp $legacy/$i/.ssh/id_rsa.pub $nfs/hadoop/edwsftpp/$backup/"$(date)"/$i/id_rsa.pub
cp $legacy/$i/.profile $nfs/hadoop/$cluster/$backup/"$(date)"/$i/.profile
done

#stuff out outside of for loop
mkdir -p $nfs/hadoop/$cluster/edwsftpp
mkdir -p $nfs/hadoop/$cluster/edwsftpp/$tmp_ssh_keys
cp $legacy/edwsftpp/.ssh/authorized_keys $nfs/hadoop/$cluster/edwsftpp/$tmp_ssh_keys/authorized_keys
#cp /ngs/app/edwsftpp/.ssh/authorized_keys /ngs150/hadoop/PHOENIX/edwsftpp/authorized_keys
echo "SOURCE - is $nfs "
cat $nfs/hadoop/$cluster/edwsftpp/$tmp_ssh_keys/authorized_keys
echo "TARGET - is $legacy"
cat $legacy/edwsftpp/.ssh/authorized_keys
