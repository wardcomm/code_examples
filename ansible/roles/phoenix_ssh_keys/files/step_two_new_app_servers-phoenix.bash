#!/bin/bash
# Lapp111 & 115 servers for all EDWAE users
legacy=/ngs/app
nfs=/ngs150
cluster=PHOENIX
tmp_ssh_keys=102_keys
for i in `more /tmp/hercules_edw_prod.yml`
do
echo $i
mkdir -p $legacy/$i/.ssh
if [ -f $legacy/$i/.ssh/id_rsa ]
then
mv $legacy/$i/.ssh/id_rsa $legacy/$i/.ssh/id_rsa.bak_"$(date +%Y-%m-%d-%H.%M.%S)"
fi

if [ -f $legacy/$i/.ssh/id_rsa.pub ]
then  mv $legacy/$i/id_rsa.pub $legacy/$i/.ssh/id_rsa.pub.bak_"$(date +%Y-%m-%d-%H.%M.%S)"
fi
ls -ltr $nfs/hadoop/$cluster/$i/$tmp_ssh_keys/
#cp $nfs/hadoop/$cluster/$i/ssh_keys/id_rsa $legacy/$i/.ssh/id_rsa
cp $nfs/hadoop/$cluster/$i/$tmp_ssh_keys/id_rsa $legacy/$i/.ssh/id_rsa
cp $nfs/hadoop/$cluster/$i/$tmp_ssh_keys/id_rsa.pub $legacy/$i/.ssh/id_rsa.pub
cp $nfs/hadoop/$cluster/$i/$tmp_ssh_keys/.profile $legacy/$i/.profile
#cp $nfs/hadoop/$cluster/$i/ssh_keys/id_rsa.pub $legacy/$i/.ssh/id_rsa.pub

chmod 600 $legacy/$i/.ssh/id_rsa
chmod 600 $legacy/$i/.ssh/id_rsa.pub
chmod 700 $legacy/$i/.ssh
chown -R $i:$i $legacy/$i/.ssh/
chown -R $i:$i $legacy/$i/.ssh/id_rsa
chown -R $i:$i $legacy/$i/.ssh/id_rsa.pub
#cat $legacy/$i/.ssh/id_rsa.pub >> ~edwsftpp/.ssh/authorized_keys

#chmod 600 ~edwsftpp/.ssh/authorized_keys
done
mkdir -p $legacy/edwsftpp/.ssh/
chmod 700 $legacy/edwsftpp/.ssh
chown -R edwsftpp:edwsftpp $legacy/edwsftpp/.ssh/
ls -ltr $nfs/hadoop/$cluster/edwsftpp/$tmp_ssh_keys/
cp $nfs/hadoop/$cluster/edwsftpp/$tmp_ssh_keys/authorized_keys $legacy/edwsftpp/.ssh/authorized_keys
ls -ltr $legacy/edwsftpp/.ssh/authorized_keys
#$nfs/hadoop/$cluster/edwsftpp/$tmp_ssh_keys/authorized_keys
chmod 644 $legacy/edwsftpp/.ssh/authorized_keys
chown -R edwsftpp:edwsftpp $legacy/edwsftpp/.ssh/authorized_keys
