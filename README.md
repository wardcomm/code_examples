

<!DOCTYPE html><html><head><meta charset="utf-8"><title>Untitled Document.md</title><style></style></head><body id="preview">
<p>Good Git Information
Command line instructions
Git global setup</p>
<p>git config --global <a href="http://user.name">user.name</a> “J. Chad Ward”
git config --global user.email <a href="mailto:%22jchadward@gmail.com">&quot;jchadward@gmail.com</a>&quot;</p>
<p>Create a new repository</p>
<p>git clone <a href="https://gitlab.com/wardcomm/code_examples.git">https://gitlab.com/wardcomm/code_examples.git</a>
cd code_examples
touch <a href="http://README.md">README.md</a>
git add <a href="http://README.md">README.md</a>
git commit -m “add README”
git push -u origin master</p>
<p>Existing folder or Git repository</p>
<p>cd existing_folder
git init
git remote add origin <a href="https://gitlab.com/wardcomm/code_examples.git">https://gitlab.com/wardcomm/code_examples.git</a>
git add .
git commit
git push -u origin master</p>

</body></html>
