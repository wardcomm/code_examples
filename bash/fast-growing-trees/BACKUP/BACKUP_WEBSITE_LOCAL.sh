#!/bin/bash
#DATA_STRUCTURE
mkdir -p /RACKBACKUP/RESTORE
mkdir -p /RACKBACKUP/BACKUP/WEEKLY
mkdir -p /RACKBACKUP/BACKUP/MONTHLY
mkdir -p /RACKBACKUP/BACKUP/DAILY
mkdir -p /RACKBACKUP/BACKUP/DAILY/LOG/
mkdir -p /RACKBACKUP/BACKUP/DAILY/Sunday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Monday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Tuesday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Wednesday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Thursday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Friday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Saturday
cat /RACKBACKUP/BACKUP/DAILY/LOG/output.log
cat /RACKBACKUP/BACKUP/DAILY/LOG/error.log
cat /RACKBACKUP/BACKUP/DAILY/LOG/data.log
cat /RACKBACKUP/BACKUP/DAILY/LOG/email.log
 #VARIABLES
LOG="/RACKBACKUP/BACKUP/DAILY/LOG/"
OUTPUT="/RACKBACKUP/BACKUP/DAILY/LOG/output.log"
ERROR="/RACKBACKUP/BACKUP/DAILY/LOG/error.log"
DATA_AFTER="/RACKBACKUP/BACKUP/DAILY/LOG/data.log"
EMAIL="/RACKBACKUP/BACKUP/DAILY/LOG/email.log"
SOURCE="/var/www/vhosts"
TARGET="/RACKBACKUP/BACKUP/DAILY/"
DATA_BEFORE="/RACKBACKUP/BACKUP/DAILY/LOG/databefore.log"
SEVEN_DAY_OLD_FILE_TARGET="/RACKBACKUP/BACKUP/DAILY/"
THE_DATE=$(date +%d%m%y%H%M)
DAYOFWEEK=$(date +"%A")
EMAIL_PERSON_1="cward@fast-growing-trees.com"
EMAIL_PERSON_2="jchadward@gmail.com"
EMAIL_PERSON_3="pmarusich@fast-growing-trees.com"
HOSTNAME="/RACKBACKUP/BACKUP/DAILY/LOG/hostname.txt"
#Beginning of script
touch $EMAIL
touch $DATA_BEFORE
touch $OUTPUT
touch $DATA_AFTER
touch $ERROR
echo "$(hostname)"
clear
#OUTPUT TO SCREEN
/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_BEFORE
echo                                         
echo                                         
echo                                         
echo                                         
echo TODAY IS $DAYOFWEEK 
echo "THE DATE IS `date`"  
echo "SERVER IS: [ $(hostname)]"
echo "++++++++++++    BACKUP REPORT   +++++++++++++"
echo                                         
echo                                         
echo CONTENTS OF DATA THAT IS TO BE DELETED 
echo ====================================== 
cat  $DATA_BEFORE                           
echo                                         
echo                                         
echo                                       
echo THE FILES THAT GOT BACKED UP ARE       
echo ======================================= 
cat $DATA_AFTER                             
clear
#OUTPUT TO EMAIL
#/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_BEFORE
echo "TODAY IS $DAYOFWEEK" >> $EMAIL
echo "THE DATE IS `date`" >> $EMAIL
echo "SERVER IS: [ $(hostname)]"  >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "++++++++++++    BACKUP REPORT   +++++++++++++" >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "CONTENTS OF DATA THAT IS TO BE DELETED"  >> $EMAIL
echo "======================================"  >> $EMAIL
cat $DATA_BEFORE                               >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "THE FILES THAT GOT BACKED UP  ARE      " >> $EMAIL
echo "=======================================" >> $EMAIL
cat $DATA_AFTER                             >> $EMAIL
sleep 5
/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_BEFORE
#CLEAN TODAY'S BACKUP DIR BEFORE BACKUP
find $TARGET$DAYOFWEEK -type f -name *WEBSITE_Backup_*.gz -exec rm -f {} \;
tar cvzPf  $TARGET$DAYOFWEEK/WEBSITE_Backup_$(date +%Y-%m-%d-%H.%M.%S).tar.gz $SOURCE > $OUTPUT 2>$ERROR
/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_AFTER
cat $EMAIL | mail -a "$OUTPUT" -a"$ERROR" -s " BACKUP COMPLETE `date` " $EMAIL_PERSON_2,$EMAIL_PERSON_1,$EMAIL_PERSON_3
#find $SEVEN_DAY_OLD_FILE_TARGET -type f -mtime +7 -exec rm -f {} \;
#CLEAN ANYTHING 7DAYS OLD FROM DAILY DIR AND BELOW
find $TARGET -type f -mtime +7 -exec rm -f {} \;
echo "FAST GROWING TREES" > $EMAIL
touch $EMAIL
touch $ERROR
touch $OUTPUT
