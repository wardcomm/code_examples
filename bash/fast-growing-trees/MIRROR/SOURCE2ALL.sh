#!/bin/bash
echo "backup server "
rsync -avz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.176.65.10:/root
echo "logstash server "
rsync -avz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.176.64.78:/root
echo "restore server "
rsync -avz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.176.5.75:/root
echo "dev-fgt-combo server "
rsync -avz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.208.160.59:/root
echo "dev-bb-combo server "
rsync -avz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.176.8.116:/root
echo "prod-bb-web server "
rsync -avz -delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.209.4.184:/root
echo "prod-bb-mysql server "
rsync -avz -delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.209.4.254:/root
echo "prod-fgt-web server "
rsync -avz -delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.223.242.233:/root
echo "prod-fgt-mysql server "
rsync -avz -delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /root/SCRIPTS/RSYNC_SOURCE/TBK-ADMIN 10.209.64.219:/root
