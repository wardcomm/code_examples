!#/bin/bash
clear
#RESTORE SCRIPT
#VARIABLES
SERVER="BACKUP"
#WEB VARIALBES
BACKUP_WEB_FILE="WEBSITE_Backup_*.tar.gz"
#"CREATE_DIRS="/proc /sys /mnt /mnt/mounted_drive"
#MYSQL CONFIG VARIABLES
BACKUP_CONFIG_FILE="MYSQLDUMP_ALL_DB_BACKUP_*.sql.gz"
BACKUP_CONFIG_LOCATION="/RACKBACKUP/RESTORE/"
#MYSQL DB RESTORE
BACKUP_DB_FILE="ALL_DATABASE_FILES_*.tar.gz"
BACKUP_DB_LOCATTION="/RACKBACKUP/RESTORE/MYSQL_DB_RESTORE"
EMAIL="/RACKBACKUP/BACKUP/DAILY/LOG/email.log"
EMAIL_PERSON_1="cward@fast-growing-trees.com"
EMAIL_PERSON_2="jchadward@gmail.com"
EMAIL_PERSON_3="pmarusich@fast-growing-trees.com"
PS3='Please enter your choice: '
options=("WEB" "MYSQL" "Quit")
#select opt in "${options[@]}"
#do
    case $1 in
        "WEB")
            echo "YOU CHOOSE WEB RESTORE"
			cd /RACKBACKUP/RESTORE 
	    
		tar xvpfz $BACKUP_WEB_FILE -C / & disown
	    echo "| WEB RESTORE COMPLETE |"
            ;;
        "MYSQL")
		echo "YOU CHOOSE MYSQL FULL DB RESTORE"
		
		#if not working check permissions on bacula account
	   gunzip < $BACKUP_CONFIG_LOCATION/$BACKUP_CONFIG_FILE | mysql -u bacula -pbacula & disown 
# cat $EMAIL | mail  -s " MYSQL RESTORE COMPLETE `date` " $EMAIL_PERSON_2,$EMAIL_PERSON_1,$EMAIL_PERSON_3
# touch $EMAIL
 echo "YOU CHOOSE MYSQL FULL DB RESTORE"
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
#done
