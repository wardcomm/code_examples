#!/bin/bash

SERVICE=$1

SA_API_USERNAME=`cat /opt/cisco/vms-installer/conf/apps.properties | grep SA_API_USERNAME | cut -d'=' -f2`
SA_API_PASSWORD=`cat /opt/cisco/vms-installer/conf/apps.properties | grep SA_API_PASSWORD | cut -d'=' -f2`
SA_API_HOST=`cf apps | grep assurance | awk '{print $6}'`

echo "SA_API_USERNAME=$SA_API_USERNAME"
echo "SA_API_PASSWORD=$SA_API_PASSWORD"
echo "SA_API_HOST=$SA_API_HOST"
echo "SERVICE=$SERVICE"

CMD="curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service?query={\"q\":[{\"field\":\"topLevelServiceId\",\"value\":\"$SERVICE\"}]}"
echo "Command is: $CMD"
echo "  -> /tmp/service.out"
echo ""
$CMD > /tmp/service.out

CMD="curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service/topology?query={\"q\":[{\"field\":\"topLevelServiceId\",\"value\":\"$SERVICE\"}]}"
echo "Command is: $CMD"
echo "  -> /tmp/service.topology.out"
echo ""
$CMD > /tmp/service.topology.out

CMD="curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service/meter?query={\"q\":[{\"field\":\"topLevelServiceId\",\"value\":\"$SERVICE\"}]}"
echo "Command is: $CMD"
echo "  -> /tmp/service.meter.out"
echo ""
$CMD > /tmp/service.meter.out

# curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service?query={"q":[{"field":"topLevelServiceId","value":"$SERVICE"}]} > /tmp/service.out

# curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service/topology?query={"q":[{"field":"topLevelServiceId","value":"$SERVICE"}]} > /tmp/service.topology.out
# curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service/meter?query={"q":[{"field":"topLevelServiceId","value":"$SERVICE"}]} > /tmp/service.meter.out

# curl -kg -u admin:iks7Ra0QqBbXWiT 'http://assuranceapi.vmsp1b.io/assurance-api/v1.0/service?query={"q":[{"field":"operationalState","value":"up"}]}' > /tmp/out
# curl -kg -u admin:iks7Ra0QqBbXWiT 'http://assuranceapi.vmsp1b.io/assurance-api/v1.0/service?query={"q":[{"field":"operationalState","value":"up","op":"ne"}]}' | python -m json.tool
