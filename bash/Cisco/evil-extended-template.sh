##
## An extended template for getting running some more enhanced commands,
## and setting up some additional useful aliases after logging into the Inception VM
##
## PRINT: is a special directive to provide some visible output to the user while
##        the login process and templates are running.
##

## These are run once and then the output is reused in the future to speed things up
PRINT: Running "cf apps" as user and saving that in /tmp/cf.apps.root ...
cf apps | grep "\.io" > /tmp/cf.apps.root
cat /tmp/cf.apps.ubuntu | grep "\.io" > /tmp/cf.apps
cat /tmp/cf.apps.root | grep "\.io" >> /tmp/cf.apps
cat /tmp/cf.apps | sort -u > /tmp/cf.apps.sorted
mv /tmp/cf.apps.sorted /tmp/cf.apps
PRINT: *** You can find the list of cf apps in file /tmp/cf.apps

## Bosh Stuff
export BOSH_FILE=/tmp/bosh.vms.txt
export TMP_BOSH=`find $$BOSH_FILE -type f -cmin -60 | wc -l`
if [ "$$TMP_BOSH" == "1" ] ; then echo "Bosh file $$BOSH_FILE is recent enough, skipping that step now..." ; else echo "no bosh file is not recent enough" ; bosh vms --dns > $$BOSH_FILE ; fi

## Nova Stuff
export NOVA_FILE=/tmp/novalist.txt
export TMP_NOVA=`find $$NOVA_FILE -type f -cmin -60 | wc -l`
if [ "$$TMP_NOVA" == "1" ] ; then echo "NOVA file $$NOVA_FILE is recent enough, skipping that step now..." ; else echo "NOVA file is NOT recent enough" ; nova list > $$NOVA_FILE ; fi

PRINT: Adding some useful aliases now...

## This is the old way of doing things... Enhanced below
##
## Quick login aliases to certain VMS
# export SME_IP=`cat $$NOVA_FILE | grep 'sme-instance' | cut -d'|' -f7 | cut -d'=' -f2 | sed 's/ //g'`
export SME_IP=`cat $$NOVA_FILE | grep 'sme-instance' | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g'`
alias SME="sshcmd cloud-user@$$SME_IP"
export SHIM_IP=`cat $$NOVA_FILE | grep shim | cut -d'|' -f7 | cut -d'=' -f2 | sed 's/ //g'`
alias SHIM="sshcmd cloud-user@$$SHIM_IP"
export NCS_ADDRESS=`cat /opt/cisco/vms-installer/conf/apps.properties | grep NCS_ADDRESS | cut -d'=' -f2`
export NCS_USERNAME=`cat /opt/cisco/vms-installer/conf/apps.properties | grep NCS_USERNAME | cut -d'=' -f2`
export NCS_PASSWORD=`cat /opt/cisco/vms-installer/conf/apps.properties | grep NCS_PASSWORD | cut -d'=' -f2`
alias NSO="echo \"Use password $$NCS_PASSWORD\" ; ssh -o StrictHostKeyChecking=no $$NCS_USERNAME@$$NCS_ADDRESS"

## Why try to remember the curl commands for SA_API?  Here you go!
export SA_API_HOST=`cat /tmp/cf.apps | grep assur | awk '{print $$1}'`
export SA_API_DOMAIN=`cat /tmp/cf.apps | grep assur | awk '{print $$6}'`
alias sa-api-service="curl -kg -u $$SA_API_USERNAME:$$SA_API_PASSWORD http://$$SA_API_DOMAIN/assurance-api/v1.0/service "
alias sa-api-service-topology="curl -kg -u $$SA_API_USERNAME:$$SA_API_PASSWORD http://$$SA_API_DOMAIN/assurance-api/v1.0/service/topology "
alias sa-api-service-meter="curl -kg -u $$SA_API_USERNAME:$$SA_API_PASSWORD http://$$SA_API_DOMAIN/assurance-api/v1.0/service/meter "

## Need to find some information about a VM or two?  This should help!
alias novalist="cat $$NOVA_FILE | cut \-d'|' \-f3,6,7 | egrep \-v \"\-\-\-|Power State|vm\-\""
alias boshlist="cat $$BOSH_FILE | egrep \-v \"\-\-\-|DNS A records|^Deployment |^Director task|^Task |^VMs total|^$$\" | cut \-d'|' \-f2,3,5,6"
alias cflist="cat /tmp/cf.apps"
alias allvms="novalist ; boshlist ; cflist"

## Oh no!  Got logged out of cf apps?  Make quick work of getting logged back in!!
alias cflogin='echo "" ; echo "User/Email is $$BOSH_USERNAME   Password is $$COMMON_PASSWORD" ; echo "API endpoint is api.$$CF_DOMAIN_ROOT     Org is vms" ; cf login --skip-ssl-validation https://api.$$CF_DOMAIN_ROOT'

## But what about Bosh?  Getting "Please choose target first"?   Or "Please login first"?
alias boshlogin='echo "Bosh username: $$BOSH_USERNAME"; echo "Bosh password: $$COMMON_PASSWORD"; echo "Bosh endpoint: $$BOSH_PRIVATE_IP"; echo "" ; echo "May need to : bosh target $$BOSH_PRIVATE_IP" ; echo ""; bosh login $$BOSH_USERNAME $$COMMON_PASSWORD; bosh target $$BOSH_PRIVATE_IP'

## NSO / NCS / ESC Stuff
alias NCSSM='echo "" ; echo "Use password $$NCS_PASSWORD" ; echo "Login with:   ssh -o StrictHostKeyChecking=no $$NCS_USERNAME@$$NCS_ADDRESS" ; echo "" ; echo "   After login..." ; echo "cd /opt/ncs/current" ; echo "source ncsrc" ; echo "ncs_cli -u admin" ; echo "" '
alias ncs-nso-shim-model='curl -i -u $$NCS_USERNAME:$$NCS_PASSWORD -X GET -H "Accept:application/vnd.yang.data+json" http://$$NCS_ADDRESS:80/api/running/devices/device/nso-shim/config/services?deep'
export ESC_ADDRESS=`cat /tmp/novalist.txt | grep "esc-" | head -1 | cut -d'|' -f7 | cut -d'=' -f2 | sed 's/ //g'`
alias ESC='echo "" ; echo "Use password $$ESC_PASSWORD" ; echo "Login with:   ssh -o StrictHostKeyChecking=no admin@$$ESC_ADDRESS" ; echo "" ; echo "" '

## A whole bunch of IP Addresses and SSH aliases
echo "STARTING the IP Address and SSH section now..."
date

export API_WORKER_0_IP=`cat $$BOSH_FILE | grep "0\.api-worker" | cut -d'|' -f5 | sed 's/ //g'`
# Permission denied (publickey)
# alias ssh-api-worker-0="sshcmd cloud-user@$$API_WORKER_0_IP"

export CASSANDRA_0_IP=`cat $$BOSH_FILE | grep "0\.cassandra\." | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-cassandra-0="sshcmd vcap@$$CASSANDRA_0_IP"
alias cqlsh-cassandra-0='cqlsh $$CASSANDRA_0_IP  -u $$CASSANDRA_USERNAME -p $$CASSANDRA_PASSWORD'

export CASSANDRA_SEED_0_IP=`cat $$BOSH_FILE | grep "0\.cassandra-seed" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-cassandra-seed-0="sshcmd vcap@$$CASSANDRA_SEED_0_IP"
alias cqlsh-cassandra-seed-0='cqlsh $$CASSANDRA_SEED_0_IP  -u $$CASSANDRA_USERNAME -p $$CASSANDRA_PASSWORD'

export CLOUD_CONTROLLER_0_IP=`cat $$BOSH_FILE | grep "0\.cloud-controller" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-cloud-controller-0="sshcmd vcap@$$CLOUD_CONTROLLER_0_IP"

export DEA_SPARE_0_IP=`cat $$BOSH_FILE | grep "0\.dea-spare" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-dea-spare-0="sshcmd vcap@$$DEA_SPARE_0_IP"

export DEA_SPARE_1_IP=`cat $$BOSH_FILE | grep "1\.dea-spare" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-dea-spare-1="sshcmd vcap@$$DEA_SPARE_1_IP"

export DEA_SPARE_2_IP=`cat $$BOSH_FILE | grep "2\.dea-spare" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-dea-spare-2="sshcmd vcap@$$DEA_SPARE_2_IP"

export HA_PROXY_0_IP=`cat $$BOSH_FILE | grep "0\.haproxy" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-haproxy-0="sshcmd vcap@$$HA_PROXY_0_IP"
alias ssh-haproxy-internal="sshcmd vcap@$$HA_PROXY_0_IP"

export KAFKA_0_IP=`cat $$BOSH_FILE | grep "0\.kafka" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-kafka-0="sshcmd vcap@$$KAFKA_0_IP"

export KAFKA_1_IP=`cat $$BOSH_FILE | grep "1\.kafka" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-kafka-1="sshcmd vcap@$$KAFKA_1_IP"

export KAFKA_2_IP=`cat $$BOSH_FILE | grep "2\.kafka" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-kafka-2="sshcmd vcap@$$KAFKA_2_IP"

export LOGGREGATOR_0_IP=`cat $$BOSH_FILE | grep "0\.loggregator\." | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-loggregator-0="sshcmd vcap@$$LOGGREGATOR_0_IP"

export LOGGREGATOR_TRAFFICCONTROLLER_0_IP=`cat $$BOSH_FILE | grep "0\.loggregator-trafficcontroller" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-loggregator-trafficcontroller-0="sshcmd vcap@$$LOGGREGATOR_TRAFFICCONTROLLER_0_IP"

export LOGSTASH_0_IP=`cat $$BOSH_FILE | grep "0\.logstash" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-logstash-0="sshcmd vcap@$$LOGSTASH_0_IP"

export LOGSTASH_1_IP=`cat $$BOSH_FILE | grep "1\.logstash" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-logstash-1="sshcmd vcap@$$LOGSTASH_1_IP"

export NFS_0_IP=`cat $$BOSH_FILE | grep "0\.nfs" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-nfs-0="sshcmd vcap@$$NFS_0_IP"

export REDIS_0_IP=`cat $$BOSH_FILE | grep "0\.redis" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-redis-0="sshcmd vcap@$$REDIS_0_IP"

export ROUTER_0_IP=`cat $$BOSH_FILE | grep "0\.router" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-router-0="sshcmd vcap@$$ROUTER_0_IP"

export ROUTER_1_IP=`cat $$BOSH_FILE | grep "1\.router" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-router-1="sshcmd vcap@$$ROUTER_1_IP"

export VAULT_CONSUL_0_IP=`cat $$BOSH_FILE | grep "0\.vault-consul" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-vault-consul-0="sshcmd vcap@$$VAULT_CONSUL_0_IP"

export ZOOKEEPER_0_IP=`cat $$BOSH_FILE | grep "0\.zookeeper" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-zookeeper-0="sshcmd vcap@$$ZOOKEEPER_0_IP"

export ZOOKEEPER_1_IP=`cat $$BOSH_FILE | grep "1\.zookeeper" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-zookeeper-1="sshcmd vcap@$$ZOOKEEPER_1_IP"

export ZOOKEEPER_2_IP=`cat $$BOSH_FILE | grep "2\.zookeeper" | cut -d'|' -f5 | sed 's/ //g'`
alias ssh-zookeeper-2="sshcmd vcap@$$ZOOKEEPER_2_IP"


# Is stash the same as logstash?
# alias stash='ssh -i /opt/cisco/vms-installer/tenant-p0/ssh/admin-key-p0 cloud-user@10.60.184.74'
#alias haproxy_0='ssh -i /opt/cisco/vms-installer/tenant-p0/ssh/admin-key-p0 vcap@#203.35.249.219'
#alias nfs='ssh -i /opt/cisco/vms-installer/tenant-p0/ssh/admin-key-p0 vcap@10.60.184.217'
#alias logstash_0='ssh -i /opt/cisco/vms-installer/tenant-p0/ssh/admin-key-p0 vcap@10.14.0.170'
#alias bosh_director='ssh -i /opt/cisco/vms-installer/tenant-p0/ssh/admin-key-p0 vcap@10.60.184.'


export SME_IP=`cat $$NOVA_FILE | grep -i "sme-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-sme='sshcmd cloud-user@$$SME_IP'

export HA_PROXY_EXTERNAL_IP=`cat $$NOVA_FILE | grep -i "haproxy-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-haproxy-external='sshcmd cloud-user@$$HA_PROXY_EXTERNAL_IP'

export NSOSHIM_IP=`cat $$NOVA_FILE | grep -i "nsoshim-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-nsoshim='sshcmd cloud-user@$$NSOSHIM_IP'

echo "At the first FIXME now..."
date

# FIXME: Copy the NCSSM alias stuff here
export NCS_SM_IP=`cat $$NOVA_FILE | grep -i "vms-ncs-.*-sm" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-ncs-sm='sshcmd cloud-user@$$NCS_SM_IP'
alias ssh-nso-sm='sshcmd cloud-user@$$NCS_SM_IP'

# FIXME: Create some NCSSM like stuff here
export NCS_DM_IP=`cat $$NOVA_FILE | grep -i "vms-ncs-.*-dm" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-ncs-dm='sshcmd cloud-user@$$NCS_DM_IP'
alias ssh-nso-dm='sshcmd cloud-user@$$NCS_DM_IP'

# FIXME: Create some NCSSM like stuff here
export ESC_1_IP=`cat $$NOVA_FILE | grep -i "vms-esc-.*-1" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-esc-1='sshcmd cloud-user@$$ESC_1_IP'

# FIXME: Create some NCSSM like stuff here
export ESC_2_IP=`cat $$NOVA_FILE | grep -i "vms-esc-.*-2" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-esc-2-dm='sshcmd cloud-user@$$ESC_2_IP'

export LICENSE_PROXY_IP=`cat $$NOVA_FILE | grep -i "license-proxy-" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-license-proxy='sshcmd cloud-user@$$LICENSE_PROXY_IP'

export OPENAM_IP=`cat $$NOVA_FILE | grep -i "openam-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-openam='sshcmd cloud-user@$$OPENAM_IP'

export OPENDJ_IP=`cat $$NOVA_FILE | grep -i "opendj-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-opendj='sshcmd cloud-user@$$OPENDJ_IP'

export GUNICORN_IP=`cat $$NOVA_FILE | grep -i "gunic.*-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-gunicorn='sshcmd cloud-user@$$GUNICORN_IP'

export MGMT_HUB_IP=`cat $$NOVA_FILE | grep -i "csr-.*-mgmt.*" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
# FIXME: Asks for password - do not have this one right yet
alias ssh-mgmt-hub='sshcmd admin@$$MGMT_HUB_IP'

export SA_ALERTING_IP=`cat $$NOVA_FILE | grep "sa-alerting-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-alerting='sshcmd cloud-user@$$SA_ALERTING_IP'
alias ssh-nagios='sshcmd cloud-user@$$SA_ALERTING_IP'

export SA_LOGGING_1_IP=`cat $$NOVA_FILE | grep "sa-logging-1-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-logging-1='sshcmd cloud-user@$$SA_LOGGING_1_IP'
alias ssh-elk-1='sshcmd cloud-user@$$SA_LOGGING_1_IP'

export SA_LOGGING_2_IP=`cat $$NOVA_FILE | grep "sa-logging-2-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-logging-2='sshcmd cloud-user@$$SA_LOGGING_2_IP'
alias ssh-elk-2='sshcmd cloud-user@$$SA_LOGGING_2_IP'

export SA_LOGGING_3_IP=`cat $$NOVA_FILE | grep "sa-logging-3-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-logging-3='sshcmd cloud-user@$$SA_LOGGING_3_IP'
alias ssh-elk-3='sshcmd cloud-user@$$SA_LOGGING_3_IP'

export SA_METRICS_IP=`cat $$NOVA_FILE | grep "sa-metrics-instance" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-metrics='sshcmd cloud-user@$$SA_METRICS_IP'
alias ssh-grafana='sshcmd cloud-user@$$SA_METRICS_IP'

export SA_THOUSAND_EYES_AGENT_IP=`cat $$NOVA_FILE | grep "sa-thousand-eyes-agent-" | cut -d'|' -f7 | sed 's/^.*app-mgmt=//i' | sed 's/,.*//' | sed 's/ //g' | sed 's/orch.*=//i'`
alias ssh-thousand-eyes-agent='echo "Try user ubuntu or Ubuntu also if this does not work!" ; sshcmd cloud-user@$$SA_THOUSAND_EYES_AGENT_IP'

export SKYFALL_URL="https://$$VMS_DOMAIN_ROOT/skyfall"

alias guitunnel='echo "" ; echo "Login with this command: " ; echo "$$SSH_LOGIN_CMD  -L 4440:$$SME_IP:4440  -L 8080:$$SA_ALERTING_IP:80  -L 8092:$$SA_LOGGING_1_IP:5601  -L 8093:$$SA_LOGGING_2_IP:5601 -L 8094:$$SA_LOGGING_3_IP:5601  -L 3000:$$SA_METRICS_IP:3000 " ; echo "" ; echo "Skyfall UI  $$SKYFALL_URL             admin password is: $$SKYFALL_SU_PASSWORD "  ; echo "            admportal / $$ADMPORTAL_PASSWORD      operator / $$OPERATOR_PASSWORD " ; echo "" ; echo "SME         https://localhost:4440         $$SME_USERNAME / $$SME_PASSWORD" ; echo "Nagios      http://localhost:8080/nagios   $$NAGIOS_USERNAME / $$NAGIOS_PASSWORD" ; echo "Kibana      http://localhost:8092          kibanaadmin / Vms%devop$$" ; echo "Grafana     http://localhost:3000          admin / Vms%devop$$" ; echo "" ; '

echo "FINISHED with the IP Address and SSH section now..."
date

alias lastone='echo "The End."'
